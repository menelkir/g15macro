G15MACRO
========

A simple Macro recording/playback app for G15Daemon.

=======
Warning
=======
I'm discontinuing this after someone made a fuzz about a feature he decided he want a decade later. 
And as far I am concerned, Arch Linux's AUR administrators find this behavior just fine, so I'm not wasting my efforts on this anymore.
I can still fix issues as I always did and help via mail, but keep in mind Arch Linux is impossible to be supported.

========
Features
========

- Records both keyboard and mouse activity, playback via 'G' hotkey.
- Up to 56 macros can be created (using the 'M' keys to select a palette).
- Each macro can have up to 128 steps

============
Requirements
============

- X11
- libg15render
- XTEST

=====
Usage
=====

To Record a macro:
------------------

1. Press the 'M' key for the palette you wish to set, if not already selected.
2. Press the MR key
3. Enter the keystrokes required
4. Press the 'G' key you want to hold the macro.

To Playback a previously recorded macro:
----------------------------------------

1. Press the 'M' key for the palette you wish to playback, if not already selected.
2. Press the 'G' key containing the macro.
